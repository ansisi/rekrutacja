import App from './components/App.js';

window.addEventListener('load', startApp);

function startApp() {
    new App().start();
}
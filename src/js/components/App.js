import { splitToLines } from "../functions/splitToLines";
import Slider from '../components/Slider.js';
import TweenMax from 'gsap';

export default class App {
    start() {
        [...document.querySelectorAll('.person__quote')].forEach( quote => splitToLines(quote, '<span>','</span>') );
        this.slider = new Slider( document.querySelector('.slider') );
        this.config();
    }

    config() {
        this.slider.onDeactivatePreviousSlide(() => {
            TweenMax.to(this.slider.getActiveSlide().querySelector('.person__quote'), .1, { opacity: 0});
            this.slider.getActiveSlide().classList.remove('active-slide');    
        });
        this.slider.onInitSlider((e) => {
            this.slider.sliderContent.classList.add('shown');
        });
        this.slider.onSlideWasChanged(() => {
            let quote = this.slider.activeSlide.querySelector('.person__quote');
            let lines = quote.querySelectorAll('span');
            TweenMax.to(quote, .1, { opacity: 1});
            TweenMax.staggerFrom(lines, 1, { opacity: 0, yPercent: 100, transformOrigin: "0% 50% 0", ease: Power2.easeOut }, .4);
        });
        this.slider.onEnterButton((e) => {
            let buttonCircle = e.target.querySelector('circle');
            let buttonPaths = e.target.querySelectorAll('path');
            TweenMax.set( buttonCircle, {transformOrigin: '50% 50%'} )
            TweenMax.to( buttonCircle, .5, {scale: .8, ease: Linear.easeNone, delay: .25} );
            TweenMax.to( buttonPaths, .5, {x: 30, ease: Power2.easeOut} );
        });   
        this.slider.onResetButton((e) => {
            let buttonCircle = e.target.querySelector('circle');
            let buttonPaths = e.target.querySelectorAll('path');
            TweenMax.to( buttonCircle, .5, {scale: 1, ease: Power2.easeOut} );
            TweenMax.to( buttonPaths, .5, {x: 0, ease: Power2.easeOut} );
        });
    }

}

export default class Slider {
    constructor(element) {
        this.element = element;
        this.sliderContent = element.querySelector('.slider__content');
        this.slides = element.querySelectorAll('.slider__slide');
        this.slidesWidth = this.slides[0].clientWidth;
        this.navButtons = element.querySelectorAll('[data-direction]');
        this.initialSlideIndex = 3;
        this.config();
    }

    config() {
        this.activateSlide(this.initialSlideIndex);
        this.run();
    }

    run() {
        this.sliderContent.addEventListener('transitionend', this.slideWasChanged.bind(this));
        [...this.navButtons].forEach( button => button.addEventListener('click', this.changeSlide.bind(this)));
        [...this.navButtons].forEach( button => button.addEventListener('mouseenter', this.enterButton.bind(this)));
        [...this.navButtons].forEach( button => button.addEventListener('mouseleave', this.resetButton.bind(this)));
    }

    activateSlide(index) {
        this.activeSlideIndex = index;
        this.activeSlide = [...this.slides].filter( slide => slide.dataset.index == index )[0];
        let activeSlideStyle = window.getComputedStyle ? getComputedStyle(this.activeSlide, null) : this.activeSlide.currentStyle;
        let marginLeft = parseInt(activeSlideStyle.marginLeft);
        let marginRight = parseInt(activeSlideStyle.marginRight);
        let sliderMiddleX = this.sliderContent.getBoundingClientRect().x + this.sliderContent.getBoundingClientRect().width / 2;
        let activeSlideMiddleX = this.activeSlide.getBoundingClientRect().x + this.activeSlide.getBoundingClientRect().width / 2 + 2 * marginLeft + 2 * marginRight;        
        this.sliderContent.style.transform = `translateX( ${sliderMiddleX - activeSlideMiddleX}px )`;
        if (this.getActiveSlide()) {
            this.onDeactivatePreviousSlide();
        }
        this.activeSlide.classList.add('active-slide');
    }

    changeSlide(e) {
        let direction = e.target.dataset.direction;
        if (direction == 'next') {
            this.activeSlideIndex = this.activeSlideIndex + 1;
        } else {
            this.activeSlideIndex = this.activeSlideIndex - 1;
        }
        this.activateSlide(this.activeSlideIndex);
        if ( this.firstSlideIsActive() || this.lastSlideIsActive() ) {
            this.lockButton(direction);
        } 
        else {
            this.unlockButtons();
        }
    }

    onDeactivatePreviousSlide(callback) {
        this.onDeactivatePreviousSlide = callback;
        return this; 
    }

    onInitSlider(callback) {
        this.onInitSlider = callback;
        return this; 
    }

    onSlideWasChanged(callback) {
        this.onSlideWasChanged = callback;
        return this; 
    }

    onEnterButton(callback, e) {
        this.onEnterButton = callback;
        return e; 
    }

    onResetButton(callback, e) {
        this.onResetButton = callback;
        return e; 
    }

    enterButton(e) {
        this.onEnterButton(e);
    }

    resetButton(e) {
        this.onResetButton(e);
    }

    slideWasChanged(e) {
        if ( e.target == this.sliderContent && e.propertyName == 'transform' ) {
            this.onSlideWasChanged();
            if ( this.slideChangedInitialTime() ) {
                this.onInitSlider();
            }
        }
    }

    lockButton(direction) {
        [...this.navButtons].filter( button => button.dataset.direction == direction )[0].classList.add('locked');
    }

    unlockButtons() {
        [...this.navButtons].forEach( button => button.classList.remove('locked'));
    }

    getActiveSlide() {
        return [...this.slides].filter( slide => slide.classList.contains('active-slide') )[0];
    }

    firstSlideIsActive() {
        return this.activeSlideIndex == 1;
    }

    lastSlideIsActive() {
        return this.activeSlideIndex == this.slides.length;
    }

    slideChangedInitialTime() {
        return !this.sliderContent.classList.contains('shown');
    }

}